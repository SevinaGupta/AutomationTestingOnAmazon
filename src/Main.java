import com.beust.ah.A;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class Main {

    public static void invokedRegistration(WebDriver driver) throws InterruptedException {
        driver.findElement(By.xpath("//*[@id=\"nav-link-accountList\"]")).click();
        driver.findElement(By.id("createAccountSubmit")).click();
        driver.findElement(By.id("ap_customer_name")).sendKeys("vk singh");
        driver.findElement(By.id("ap_email")).sendKeys("vksingh123@gmail.com");
        driver.findElement(By.id("ap_password")).sendKeys("Hello@3031");
        driver.findElement(By.id("ap_password_check")).sendKeys("Hello@3031");
        driver.findElement(By.id("continue")).click();
        Thread.sleep(1000);
    }

    public static void invokedLogin(WebDriver driver) throws InterruptedException {
            driver.findElement(By.xpath("//*[@id=\"nav-link-accountList\"]")).click();
            driver.findElement(By.id("ap_email")).sendKeys("sevinagupta@gmail.com");
            driver.findElement(By.id("continue")).click();
            driver.findElement(By.id("ap_password")).sendKeys("Sevina@8898");
            driver.findElement(By.id("signInSubmit")).click();
           Thread.sleep(1000);
    }

    public static void invokedMouseHover(WebDriver driver) throws InterruptedException {
            WebElement element = driver.findElement(By.xpath("//*[@id=\"nav-link-accountList\"]"));
            Actions actions = new Actions(driver);
            actions.moveToElement(element).build().perform();
            driver.findElement(By.linkText("Orders")).click();
            Thread.sleep(1000);
    }

    public static void invokedScrollAction(WebDriver driver) throws InterruptedException {
            JavascriptExecutor js = (JavascriptExecutor)driver;
//          Scroll By Pixels
            js.executeScript("window.scrollBy(0,3000)");
            Thread.sleep(1000);
    }

    public static void invokedSearch(WebDriver driver) throws InterruptedException {
            driver.findElement(By.id("twotabsearchtextbox")).click();
            driver.findElement(By.id("twotabsearchtextbox")).sendKeys("Acer Aspire 5 A515-46-R3UB");
            driver.findElement(By.id("nav-search-submit-button")).click();
            Thread.sleep(1000);
    }

    public static void invokedAddToCart(WebDriver driver) throws InterruptedException {
            driver.findElement(By.xpath("//*[@id=\"search\"]/div[1]/div[1]/div/span[3]/div[2]/div[4]/div/div/div/div/div/div[2]/div/div/div[1]/h2/a/span")).click();
            driver.findElement(By.xpath("//*[@id=\"add-to-cart-button\"]")).click();
            Thread.sleep(1000);
    }

    public static void invokedBuyProduct(WebDriver driver) throws InterruptedException {
            driver.findElement(By.xpath("//*[@id=\"attach-sidesheet-view-cart-button\"]/span/input")).click();
            driver.findElement(By.xpath("//*[@id=\"sc-buy-box-ptc-button\"]/span/input")).click();
//            Thread.sleep(1000);
    }

    public static void main(String[] args) throws Exception {
        System.setProperty("webdriver.chrome.driver", "/home/crazyninja/Downloads/chromedriver");
        WebDriver driver = new ChromeDriver();
        driver.get("https://www.amazon.com");
        driver.manage().window().maximize();
        driver.manage().timeouts().getImplicitWaitTimeout();
        driver.manage().timeouts().getPageLoadTimeout();

//      calling all methods
//        invokedRegistration(driver);
//        invokedLogin(driver);
//        invokedMouseHover(driver);
        invokedScrollAction(driver);
        invokedSearch(driver);
        invokedAddToCart(driver);
        invokedBuyProduct(driver);
    }

}
